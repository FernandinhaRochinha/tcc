﻿using MySql.Data.MySqlClient;
using Nsf._2018.Modulo3.App.DB.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.App.DB.Pedido
{
    class PedidoDatabaseItem
    {
        public int Salvar(PedidoItemDTO dto)
        {
            string script = @"INSERT INTO tb_pedido_item (id_produto, id_pedido) VALUES (@id_produto, @id_pedido)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_produto", dto.IdProduto));
            parms.Add(new MySqlParameter("id_pedido", dto.IdPedido));

            Database db = new Database();

            int pk= db.ExecuteInsertScriptWithPk(script, parms);
            return pk; 
        }
        public void Remover (int id)
        {
            string script = @"DELETE FROM tb_pedido_item where id_pedido_item = @id_pedido_item ";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_pedido_item", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }
           
    }
}
