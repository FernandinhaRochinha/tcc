﻿using Nsf._2018.Modulo3.App.DB.Produto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.App.DB.Produto
{
     class ProdutoBusiness
    {
        public int Salvar (ProdutoDTO dto)
        {
            if (dto.Produto == string.Empty)
            {
                throw new ArgumentException("Produto é obrigatorio");
            
            }
            if (dto.Preco == 0)
            {
                throw new ArgumentException("Produto é obrigatorio");


            }
            ProdutoDatabase db = new ProdutoDatabase();
            return db.Salvar(dto);
        }

        public List<ProdutoDTO> Listar()
        {
            ProdutoDatabase db = new ProdutoDatabase();
            return db.Listar();
        }


    }
}
