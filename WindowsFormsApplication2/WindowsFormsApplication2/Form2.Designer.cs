﻿namespace WindowsFormsApplication2
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.menuStrip2 = new System.Windows.Forms.MenuStrip();
            this.cadastrosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clientesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fornecedorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.uFToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.departamentoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.veíciulosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modeloToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fabricanteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.grupomodeloToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.códigogrupomodeloToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.produtosOficinaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.produtosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.serviçosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aplicalidadeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fabriprodutoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.produtofornecedorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.usuárioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.perfilToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.orçamentoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ordemserviçoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.itemprodutoorçamentoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.itemserviçoorçamentoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripTextBox1 = new System.Windows.Forms.ToolStripMenuItem();
            this.orçamentoToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.ordemDeServiçoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.calculadoraToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.caléndarioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sairToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ajudaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.produtosToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.serviçosToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.marcasEModelosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.unidadesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logoffDeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sairToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip2.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Location = new System.Drawing.Point(0, 31);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(648, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // menuStrip2
            // 
            this.menuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem3,
            this.veíciulosToolStripMenuItem,
            this.produtosOficinaToolStripMenuItem,
            this.toolStripMenuItem1,
            this.toolStripMenuItem2,
            this.cadastrosToolStripMenuItem,
            this.ajudaToolStripMenuItem});
            this.menuStrip2.Location = new System.Drawing.Point(0, 0);
            this.menuStrip2.Name = "menuStrip2";
            this.menuStrip2.Size = new System.Drawing.Size(648, 31);
            this.menuStrip2.TabIndex = 1;
            this.menuStrip2.Text = "menuStrip2";
            this.menuStrip2.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.menuStrip2_ItemClicked);
            // 
            // cadastrosToolStripMenuItem
            // 
            this.cadastrosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.clientesToolStripMenuItem,
            this.fornecedorToolStripMenuItem,
            this.uFToolStripMenuItem,
            this.departamentoToolStripMenuItem,
            this.orçamentoToolStripMenuItem1,
            this.ordemDeServiçoToolStripMenuItem,
            this.calculadoraToolStripMenuItem,
            this.caléndarioToolStripMenuItem,
            this.toolStripTextBox1,
            this.sairToolStripMenuItem});
            this.cadastrosToolStripMenuItem.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cadastrosToolStripMenuItem.Name = "cadastrosToolStripMenuItem";
            this.cadastrosToolStripMenuItem.Size = new System.Drawing.Size(111, 27);
            this.cadastrosToolStripMenuItem.Text = "Ferramentas";
            this.cadastrosToolStripMenuItem.Click += new System.EventHandler(this.cadastrosToolStripMenuItem_Click);
            // 
            // clientesToolStripMenuItem
            // 
            this.clientesToolStripMenuItem.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clientesToolStripMenuItem.Name = "clientesToolStripMenuItem";
            this.clientesToolStripMenuItem.Size = new System.Drawing.Size(160, 24);
            this.clientesToolStripMenuItem.Text = "cliente";
            // 
            // fornecedorToolStripMenuItem
            // 
            this.fornecedorToolStripMenuItem.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fornecedorToolStripMenuItem.Name = "fornecedorToolStripMenuItem";
            this.fornecedorToolStripMenuItem.Size = new System.Drawing.Size(160, 24);
            this.fornecedorToolStripMenuItem.Text = "fornecedor";
            // 
            // uFToolStripMenuItem
            // 
            this.uFToolStripMenuItem.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uFToolStripMenuItem.Name = "uFToolStripMenuItem";
            this.uFToolStripMenuItem.Size = new System.Drawing.Size(200, 28);
            this.uFToolStripMenuItem.Text = "veículo";
            // 
            // departamentoToolStripMenuItem
            // 
            this.departamentoToolStripMenuItem.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.departamentoToolStripMenuItem.Name = "departamentoToolStripMenuItem";
            this.departamentoToolStripMenuItem.Size = new System.Drawing.Size(200, 28);
            this.departamentoToolStripMenuItem.Text = "produtos";
            // 
            // veíciulosToolStripMenuItem
            // 
            this.veíciulosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem4,
            this.modeloToolStripMenuItem,
            this.fabricanteToolStripMenuItem,
            this.grupomodeloToolStripMenuItem,
            this.códigogrupomodeloToolStripMenuItem});
            this.veíciulosToolStripMenuItem.Font = new System.Drawing.Font("Lucida Fax", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.veíciulosToolStripMenuItem.Name = "veíciulosToolStripMenuItem";
            this.veíciulosToolStripMenuItem.Size = new System.Drawing.Size(111, 27);
            this.veíciulosToolStripMenuItem.Text = "Movimento";
            this.veíciulosToolStripMenuItem.Click += new System.EventHandler(this.veíciulosToolStripMenuItem_Click);
            // 
            // modeloToolStripMenuItem
            // 
            this.modeloToolStripMenuItem.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.modeloToolStripMenuItem.Name = "modeloToolStripMenuItem";
            this.modeloToolStripMenuItem.Size = new System.Drawing.Size(237, 24);
            this.modeloToolStripMenuItem.Text = "modelo";
            // 
            // fabricanteToolStripMenuItem
            // 
            this.fabricanteToolStripMenuItem.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fabricanteToolStripMenuItem.Name = "fabricanteToolStripMenuItem";
            this.fabricanteToolStripMenuItem.Size = new System.Drawing.Size(237, 24);
            this.fabricanteToolStripMenuItem.Text = "fabricante";
            // 
            // grupomodeloToolStripMenuItem
            // 
            this.grupomodeloToolStripMenuItem.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grupomodeloToolStripMenuItem.Name = "grupomodeloToolStripMenuItem";
            this.grupomodeloToolStripMenuItem.Size = new System.Drawing.Size(237, 24);
            this.grupomodeloToolStripMenuItem.Text = "grupo_modelo";
            // 
            // códigogrupomodeloToolStripMenuItem
            // 
            this.códigogrupomodeloToolStripMenuItem.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.códigogrupomodeloToolStripMenuItem.Name = "códigogrupomodeloToolStripMenuItem";
            this.códigogrupomodeloToolStripMenuItem.Size = new System.Drawing.Size(237, 24);
            this.códigogrupomodeloToolStripMenuItem.Text = "código_grupo_modelo";
            // 
            // produtosOficinaToolStripMenuItem
            // 
            this.produtosOficinaToolStripMenuItem.Name = "produtosOficinaToolStripMenuItem";
            this.produtosOficinaToolStripMenuItem.Size = new System.Drawing.Size(12, 23);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.produtosToolStripMenuItem,
            this.serviçosToolStripMenuItem,
            this.aplicalidadeToolStripMenuItem,
            this.fabriprodutoToolStripMenuItem,
            this.produtofornecedorToolStripMenuItem});
            this.toolStripMenuItem1.Font = new System.Drawing.Font("Lucida Fax", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(100, 27);
            this.toolStripMenuItem1.Text = "Relatórios";
            // 
            // produtosToolStripMenuItem
            // 
            this.produtosToolStripMenuItem.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.produtosToolStripMenuItem.Name = "produtosToolStripMenuItem";
            this.produtosToolStripMenuItem.Size = new System.Drawing.Size(186, 24);
            this.produtosToolStripMenuItem.Text = "produtos";
            // 
            // serviçosToolStripMenuItem
            // 
            this.serviçosToolStripMenuItem.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.serviçosToolStripMenuItem.Name = "serviçosToolStripMenuItem";
            this.serviçosToolStripMenuItem.Size = new System.Drawing.Size(186, 24);
            this.serviçosToolStripMenuItem.Text = "serviços";
            // 
            // aplicalidadeToolStripMenuItem
            // 
            this.aplicalidadeToolStripMenuItem.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.aplicalidadeToolStripMenuItem.Name = "aplicalidadeToolStripMenuItem";
            this.aplicalidadeToolStripMenuItem.Size = new System.Drawing.Size(186, 24);
            this.aplicalidadeToolStripMenuItem.Text = "aplicalidade";
            // 
            // fabriprodutoToolStripMenuItem
            // 
            this.fabriprodutoToolStripMenuItem.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fabriprodutoToolStripMenuItem.Name = "fabriprodutoToolStripMenuItem";
            this.fabriprodutoToolStripMenuItem.Size = new System.Drawing.Size(186, 24);
            this.fabriprodutoToolStripMenuItem.Text = "fabri_produto";
            // 
            // produtofornecedorToolStripMenuItem
            // 
            this.produtofornecedorToolStripMenuItem.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.produtofornecedorToolStripMenuItem.Name = "produtofornecedorToolStripMenuItem";
            this.produtofornecedorToolStripMenuItem.Size = new System.Drawing.Size(186, 24);
            this.produtofornecedorToolStripMenuItem.Text = "produto_fornecedor";
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.usuárioToolStripMenuItem,
            this.perfilToolStripMenuItem});
            this.toolStripMenuItem2.Font = new System.Drawing.Font("Lucida Fax", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(98, 27);
            this.toolStripMenuItem2.Text = "Utilitários";
            // 
            // usuárioToolStripMenuItem
            // 
            this.usuárioToolStripMenuItem.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.usuárioToolStripMenuItem.Name = "usuárioToolStripMenuItem";
            this.usuárioToolStripMenuItem.Size = new System.Drawing.Size(152, 24);
            this.usuárioToolStripMenuItem.Text = "usuário";
            // 
            // perfilToolStripMenuItem
            // 
            this.perfilToolStripMenuItem.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.perfilToolStripMenuItem.Name = "perfilToolStripMenuItem";
            this.perfilToolStripMenuItem.Size = new System.Drawing.Size(152, 24);
            this.perfilToolStripMenuItem.Text = "perfil";
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.orçamentoToolStripMenuItem,
            this.ordemserviçoToolStripMenuItem,
            this.itemprodutoorçamentoToolStripMenuItem,
            this.itemserviçoorçamentoToolStripMenuItem,
            this.produtosToolStripMenuItem1,
            this.serviçosToolStripMenuItem1,
            this.marcasEModelosToolStripMenuItem,
            this.unidadesToolStripMenuItem,
            this.logoffDeToolStripMenuItem,
            this.sairToolStripMenuItem1});
            this.toolStripMenuItem3.Font = new System.Drawing.Font("Lucida Fax", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(99, 27);
            this.toolStripMenuItem3.Text = "Cadastros";
            // 
            // orçamentoToolStripMenuItem
            // 
            this.orçamentoToolStripMenuItem.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.orçamentoToolStripMenuItem.Name = "orçamentoToolStripMenuItem";
            this.orçamentoToolStripMenuItem.Size = new System.Drawing.Size(222, 24);
            this.orçamentoToolStripMenuItem.Text = "cliente";
            // 
            // ordemserviçoToolStripMenuItem
            // 
            this.ordemserviçoToolStripMenuItem.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ordemserviçoToolStripMenuItem.Name = "ordemserviçoToolStripMenuItem";
            this.ordemserviçoToolStripMenuItem.Size = new System.Drawing.Size(222, 24);
            this.ordemserviçoToolStripMenuItem.Text = "fornecedor";
            this.ordemserviçoToolStripMenuItem.Click += new System.EventHandler(this.ordemserviçoToolStripMenuItem_Click);
            // 
            // itemprodutoorçamentoToolStripMenuItem
            // 
            this.itemprodutoorçamentoToolStripMenuItem.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.itemprodutoorçamentoToolStripMenuItem.Name = "itemprodutoorçamentoToolStripMenuItem";
            this.itemprodutoorçamentoToolStripMenuItem.Size = new System.Drawing.Size(222, 24);
            this.itemprodutoorçamentoToolStripMenuItem.Text = "funcionários";
            // 
            // itemserviçoorçamentoToolStripMenuItem
            // 
            this.itemserviçoorçamentoToolStripMenuItem.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.itemserviçoorçamentoToolStripMenuItem.Name = "itemserviçoorçamentoToolStripMenuItem";
            this.itemserviçoorçamentoToolStripMenuItem.Size = new System.Drawing.Size(222, 24);
            this.itemserviçoorçamentoToolStripMenuItem.Text = "veículos";
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(203, 24);
            this.toolStripMenuItem4.Text = "veículo";
            this.toolStripMenuItem4.Click += new System.EventHandler(this.toolStripMenuItem4_Click);
            // 
            // toolStripTextBox1
            // 
            this.toolStripTextBox1.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripTextBox1.Name = "toolStripTextBox1";
            this.toolStripTextBox1.Size = new System.Drawing.Size(200, 28);
            this.toolStripTextBox1.Text = "funcionário";
            this.toolStripTextBox1.Click += new System.EventHandler(this.toolStripTextBox1_Click);
            // 
            // orçamentoToolStripMenuItem1
            // 
            this.orçamentoToolStripMenuItem1.Name = "orçamentoToolStripMenuItem1";
            this.orçamentoToolStripMenuItem1.Size = new System.Drawing.Size(200, 28);
            this.orçamentoToolStripMenuItem1.Text = "orçamento";
            // 
            // ordemDeServiçoToolStripMenuItem
            // 
            this.ordemDeServiçoToolStripMenuItem.Name = "ordemDeServiçoToolStripMenuItem";
            this.ordemDeServiçoToolStripMenuItem.Size = new System.Drawing.Size(200, 28);
            this.ordemDeServiçoToolStripMenuItem.Text = "ordem de serviço";
            // 
            // calculadoraToolStripMenuItem
            // 
            this.calculadoraToolStripMenuItem.Name = "calculadoraToolStripMenuItem";
            this.calculadoraToolStripMenuItem.Size = new System.Drawing.Size(200, 28);
            this.calculadoraToolStripMenuItem.Text = "calculadora";
            // 
            // caléndarioToolStripMenuItem
            // 
            this.caléndarioToolStripMenuItem.Name = "caléndarioToolStripMenuItem";
            this.caléndarioToolStripMenuItem.Size = new System.Drawing.Size(200, 28);
            this.caléndarioToolStripMenuItem.Text = "caléndario";
            // 
            // sairToolStripMenuItem
            // 
            this.sairToolStripMenuItem.Name = "sairToolStripMenuItem";
            this.sairToolStripMenuItem.Size = new System.Drawing.Size(200, 28);
            this.sairToolStripMenuItem.Text = "sair";
            // 
            // ajudaToolStripMenuItem
            // 
            this.ajudaToolStripMenuItem.Name = "ajudaToolStripMenuItem";
            this.ajudaToolStripMenuItem.Size = new System.Drawing.Size(50, 27);
            this.ajudaToolStripMenuItem.Text = "Ajuda";
            // 
            // produtosToolStripMenuItem1
            // 
            this.produtosToolStripMenuItem1.Name = "produtosToolStripMenuItem1";
            this.produtosToolStripMenuItem1.Size = new System.Drawing.Size(222, 24);
            this.produtosToolStripMenuItem1.Text = "produtos";
            // 
            // serviçosToolStripMenuItem1
            // 
            this.serviçosToolStripMenuItem1.Name = "serviçosToolStripMenuItem1";
            this.serviçosToolStripMenuItem1.Size = new System.Drawing.Size(222, 24);
            this.serviçosToolStripMenuItem1.Text = "serviços";
            // 
            // marcasEModelosToolStripMenuItem
            // 
            this.marcasEModelosToolStripMenuItem.Name = "marcasEModelosToolStripMenuItem";
            this.marcasEModelosToolStripMenuItem.Size = new System.Drawing.Size(222, 24);
            this.marcasEModelosToolStripMenuItem.Text = "marcas e modelos";
            // 
            // unidadesToolStripMenuItem
            // 
            this.unidadesToolStripMenuItem.Name = "unidadesToolStripMenuItem";
            this.unidadesToolStripMenuItem.Size = new System.Drawing.Size(222, 24);
            this.unidadesToolStripMenuItem.Text = "unidades";
            // 
            // logoffDeToolStripMenuItem
            // 
            this.logoffDeToolStripMenuItem.Name = "logoffDeToolStripMenuItem";
            this.logoffDeToolStripMenuItem.Size = new System.Drawing.Size(222, 24);
            this.logoffDeToolStripMenuItem.Text = "logoff de";
            // 
            // sairToolStripMenuItem1
            // 
            this.sairToolStripMenuItem1.Name = "sairToolStripMenuItem1";
            this.sairToolStripMenuItem1.Size = new System.Drawing.Size(222, 24);
            this.sairToolStripMenuItem1.Text = "sair";
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(648, 287);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.menuStrip2);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form2";
            this.Text = "Form2";
            this.Load += new System.EventHandler(this.Form2_Load);
            this.menuStrip2.ResumeLayout(false);
            this.menuStrip2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.MenuStrip menuStrip2;
        private System.Windows.Forms.ToolStripMenuItem cadastrosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clientesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fornecedorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem uFToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem departamentoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem veíciulosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem modeloToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fabricanteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem grupomodeloToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem códigogrupomodeloToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem produtosOficinaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem produtosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem serviçosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aplicalidadeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fabriprodutoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem produtofornecedorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem usuárioToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem perfilToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem orçamentoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ordemserviçoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem itemprodutoorçamentoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem itemserviçoorçamentoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem produtosToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem serviçosToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem marcasEModelosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem unidadesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem logoffDeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sairToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem orçamentoToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem ordemDeServiçoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem calculadoraToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem caléndarioToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripTextBox1;
        private System.Windows.Forms.ToolStripMenuItem sairToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ajudaToolStripMenuItem;
    }
}